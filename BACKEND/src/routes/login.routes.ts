import {Router, Request, Response} from 'express';
import Container from 'typedi';
import {LoginController} from '../controller/LoginController';

const loginRouter = Router();
const controller: LoginController = Container.get(LoginController);

loginRouter.post('/', (req: Request, res: Response) =>
  controller.login(req, res));

export default loginRouter;
