import {Router, Request, Response} from 'express';
import Container from 'typedi';
import {CardController} from '../controller/CardController';
import logOperation from '../middleware/OperationMiddleware';
import validateBody from '../middleware/ValidationMiddleware';

const cardsRouter = Router();
const controller: CardController = Container.get(CardController);

cardsRouter.get(
    '/',
    (req: Request, res: Response) => controller.findCards(req, res));

cardsRouter.post(
    '/',
    validateBody,
    (req: Request, res: Response) => controller.createCard(req, res));

cardsRouter.put(
    '/:id',
    validateBody,
    logOperation,
    (req: Request, res: Response) => controller.updateCard(req, res));

cardsRouter.delete(
    '/:id',
    logOperation,
    (req: Request, res: Response) => controller.deleteCard(req, res));

export default cardsRouter;
