import {Request, Response} from 'express';
import {Service} from 'typedi';
import {CardService} from '../service/CardService';
import {sendErrorResponse} from '../common/RestUtils';

@Service()
export class CardController {
  constructor(private service: CardService) {}

  public async findCards(_: Request, res: Response): Promise<void> {
    const card = await this.service.findAllCards();
    res.status(200).json(card);
  }

  public async createCard(req: Request, res: Response): Promise<void> {
    const card = await this.service.createCard(req.body);
    res.status(201).json(card);
  }

  public async updateCard(req: Request, res: Response): Promise<void> {
    try {
      const card = await this.service.updateCard(req.body);
      res.status(200).json(card);
    } catch {
      this.sendNotFound(res);
    }
  }

  public async deleteCard(req: Request, res: Response): Promise<void> {
    const id: string = req.params.id;

    try {
      const cards = await this.service.deleteCard(id);
      res.status(200).json(cards);
    } catch {
      this.sendNotFound(res);
    }
  }

  private sendNotFound(res: Response) {
    sendErrorResponse(404, res);
  }
}
