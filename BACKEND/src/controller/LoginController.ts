import {Request, Response} from 'express';
import {Service} from 'typedi';
import {LoginService} from '../service/LoginService';

@Service()
export class LoginController {
  constructor(private service: LoginService) {}

  public login(req: Request, res: Response): void {
    if (this.service.checkCredentials(req.body)) {
      res.status(201).json(this.service.createJWT());
    }
    res.status(401).send();
  }
}
