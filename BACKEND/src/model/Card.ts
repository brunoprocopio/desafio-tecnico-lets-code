import {IsNotEmpty} from 'class-validator';
import {Schema, model} from 'mongoose';
import {v4 as uuidv4} from 'uuid';

export interface Card {
  id?: string;
  titulo: string;
  conteudo: string;
  lista: string;
}

export class CardDTO implements Card {
  @IsNotEmpty()
  titulo: string;

  @IsNotEmpty()
  conteudo: string;

  @IsNotEmpty()
  lista: string;

  constructor(
      titulo: string,
      conteudo: string,
      lista: string) {
    this.titulo = titulo;
    this.conteudo = conteudo;
    this.lista = lista;
  }
}

export const CardSchema = new Schema<Card>({
  _id: {type: String, default: uuidv4},
  titulo: {type: String, required: true},
  conteudo: {type: String, required: true},
  lista: {type: String, required: true},
}, {versionKey: false}).set(
    'toJSON', {
      virtuals: true,
      versionKey: false,
      transform: function(_: any, ret: any) {
        delete ret._id;
      },
    },
);

export const CardModel = model<Card>('Card', CardSchema);
