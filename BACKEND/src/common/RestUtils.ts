import {Response} from 'express';

export function sendErrorResponse(statusCode: number, res: Response) {
  res.status(statusCode).send();
}
