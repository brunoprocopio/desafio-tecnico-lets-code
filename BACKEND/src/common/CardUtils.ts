import {Card, CardDTO} from '../model/Card';

export function toCardDTO(card: Card): CardDTO {
  return new CardDTO(card.titulo, card.conteudo, card.lista);
}
