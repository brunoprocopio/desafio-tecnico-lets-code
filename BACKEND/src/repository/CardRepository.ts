import {Service} from 'typedi';
import {Card, CardModel} from '../model/Card';

@Service()
class CardRepository {
  public async select(): Promise<Card[]> {
    return CardModel.find();
  }

  public async selectOne(id: string): Promise<Card | null> {
    return CardModel.findById(id);
  }

  public async create(card: Card): Promise<Card> {
    return new CardModel(card).save();
  }

  public async update(card: Card): Promise<Card | null> {
    return CardModel.findByIdAndUpdate(card.id, card, {new: true});
  }

  public async delete(cardId: string): Promise<Card | null> {
    return CardModel.findByIdAndRemove(cardId);
  }
}

export default CardRepository;
