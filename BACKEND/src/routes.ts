import {Router} from 'express';
import cardsRouter from './routes/card.routes';
import bearerToken from 'express-bearer-token';
import checkAutorization from './middleware/AuthorizationMiddleware';
import loginRouter from './routes/login.routes';

const routes = Router();

routes.use(bearerToken());
routes.use('/login', loginRouter);
routes.use(checkAutorization);
routes.use('/cards', cardsRouter);

export default routes;
