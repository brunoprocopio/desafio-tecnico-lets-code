import {Request, Response, NextFunction} from 'express';
import {Card} from '../model/Card';
import dateformat from 'dateformat';
import {CardService} from '../service/CardService';
import Container from 'typedi';
import {sendErrorResponse} from '../common/RestUtils';

const cardService: CardService = Container.get(CardService);

function getDate(): string {
  const now = new Date();
  return dateformat(now, 'dd/mm/yyyy HH:MM:ss');
}

function getCardId(req: Request): string {
  return req.params.id;
}

async function getCard(id: string): Promise<Card> {
  return await cardService.findCard(id);
}

function getOperation(req: Request): string {
  switch (req.method) {
    case 'PUT':
      return 'Alterar';
    case 'DELETE':
      return 'Remover';
    default:
      return '';
  }
}

export default async function logOperation(
    req: Request,
    res: Response,
    next: NextFunction): Promise<void> {
  try {
    const cardId = getCardId(req);
    const card = await getCard(cardId);
    const operation = getOperation(req);

    console.log(
        `${getDate()} - Card ${card.id} - ${card.titulo} - ${operation}`);
    next();
  } catch {
    sendErrorResponse(404, res);
  }
}
