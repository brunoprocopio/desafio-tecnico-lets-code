import {Request, Response, NextFunction} from 'express';
import Container from 'typedi';
import {LoginService} from '../service/LoginService';

const service: LoginService = Container.get(LoginService);

export default function checkAutorization(
    req: Request, res: Response, next: NextFunction): void {
  if (service.verifyJWT(req.token)) {
    next();
  } else {
    res.status(401).send();
  }
}
