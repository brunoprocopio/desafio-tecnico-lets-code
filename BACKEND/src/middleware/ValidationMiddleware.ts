import {validate} from 'class-validator';
import {Request, Response, NextFunction} from 'express';
import {toCardDTO} from '../common/CardUtils';
import {sendErrorResponse} from '../common/RestUtils';

async function validateCardDTO(req: Request) : Promise<boolean> {
  try {
    const cardDto = toCardDTO(req.body);
    const validationErrors = await validate(cardDto);
    const cardId = req.body.id;

    return (validationErrors.length === 0 && req.params.id === cardId);
  } catch {
    return false;
  }
}

export default async function validateBody(
    req: Request,
    res: Response,
    next: NextFunction): Promise<void> {
  if (await validateCardDTO(req)) {
    next();
  } else {
    sendErrorResponse(400, res);
  }
}
