import {Service} from 'typedi';
import jwt from 'jsonwebtoken';
import {Authentication} from '../model/Authentication';

@Service()
export class LoginService {
  private login = process.env.LOGIN;
  private senha = process.env.SENHA;
  private jwtSecret = process.env.JWT_SECRET || '';

  public checkCredentials(auth: Authentication): boolean {
    return this.login !== undefined && this.senha !== undefined &&
    auth.login === this.login && auth.senha === this.senha;
  }

  public createJWT() {
    if (this.jwtSecret && this.login) {
      return jwt.sign({login: this.login}, this.jwtSecret, {expiresIn: 300});
    } else {
      throw new Error();
    }
  }

  public verifyJWT(token: string | undefined): boolean {
    if (!token) {
      return false;
    }

    try {
      jwt.verify(token, this.jwtSecret);
      return true;
    } catch {
      return false;
    }
  }
}

