import {Service} from 'typedi';
import {Card} from '../model/Card';
import CardRepository from '../repository/CardRepository';

@Service()
export class CardService {
  constructor(private repository: CardRepository) {}

  public async findAllCards(): Promise<Card[]> {
    return await this.repository.select();
  }

  public async findCard(cardId: string): Promise<Card> {
    const card = await this.repository.selectOne(cardId);

    if (card) {
      return card;
    } else {
      throw new Error();
    }
  }

  public async createCard(card: Card): Promise<Card> {
    return await this.repository.create(card);
  }

  public async updateCard(card: Card): Promise<Card> {
    const updatedCard = await this.repository.update(card);

    if (updatedCard) {
      return updatedCard;
    } else {
      throw new Error();
    }
  }

  public async deleteCard(cardId: string): Promise<Card[]> {
    const deletedCard = await this.repository.delete(cardId);

    if (deletedCard) {
      return this.findAllCards();
    } else {
      throw new Error();
    }
  }
}

