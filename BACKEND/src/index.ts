import {config} from 'dotenv-safe';
config();

import 'reflect-metadata';
import express from 'express';
import {connect} from 'mongoose';
import routes from './routes';
import {json} from 'body-parser';
import cors from 'cors';

connect(process.env.DB_CONNECTION_STRING || '', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

const app = express();

app.use(json());
app.use(cors());
app.use(routes);

app.listen(process.env.PORT || 5000, () => {});
