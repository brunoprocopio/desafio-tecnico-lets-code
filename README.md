# Desafio Técnico - Backend

## Feito por Bruno Procopio (brunoprocopio59@gmail.com)

## Rodando a aplicação

A aplicação espera um arquivo de configuração com as variáveis de ambiente (.env), no root da past BACKEND. Existe um arquivo de exemplo sem os valores preenchidos (.env.example).

```
PORT=
DB_CONNECTION_STRING=
JWT_SECRET=
LOGIN=
SENHA=
```

A DB_CONNECTION_STRING é uma string de conexão com o MongoDB. Para esse projeto já foi configurado um docker-compose, então como valor padrão pra conectar nesse container favor preencher com:

```
DB_CONNECTION_STRING=mongodb://mongodb:27017/lets-code
```

A porta padrão que está configurada e exposta no docker-compose é a 5000.

## docker-compose.yml

```
version: "3.9"
services:
  mongodb:
    image: "mongo"
    ports:
        - "27017:27017"
  backend:
    build: BACKEND/.
    depends_on:
      - mongodb
    ports:
        - "5000:5000"
  front:
    build: FRONT/.
    depends_on:
      - backend
    ports:
        - "3000:5000"
```

Aqui foi exposta a porta 5000 do container para poder ser acessado pela porta esperada no front. Caso seja alterado o nome do container do mongodb alterar também na string de conexão no .env.

O front fica disponibilizado na porta 3000 quando executado pelo docker-compose up.